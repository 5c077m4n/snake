import random
import curses
from curses import wrapper


def start_game(stdscr):
    curses.curs_set(0)
    screen_height, screen_width = stdscr.getmaxyx()

    window = curses.newwin(screen_height, screen_width, 0, 0)
    window.clear()
    window.keypad(True)
    window.timeout(100)

    snake_x = random.randint(1, screen_width / 2)
    snake_y = random.randint(1, screen_height - 1)
    snake = [
        [snake_y, snake_x],
        [snake_y, snake_x - 1],
        [snake_y, snake_x - 2]
    ]

    food = [int(screen_height/2), int(screen_width/2)]
    window.addch(food[0], food[1], curses.ACS_PI)

    key = curses.KEY_RIGHT

    while True:
        next_key = window.getch()
        key = key if next_key == -1 else next_key

        if snake[0][0] in [0, screen_height - 1] or snake[0][1] in [0, screen_width - 1] or snake[0] in snake[1:]:
            return len(snake) - 3

        new_head = [snake[0][0], snake[0][1]]

        if key == curses.KEY_UP:
            new_head[0] -= 1
        elif key == curses.KEY_DOWN:
            new_head[0] += 1
        elif key == curses.KEY_RIGHT:
            new_head[1] += 1
        elif key == curses.KEY_LEFT:
            new_head[1] -= 1

        snake.insert(0, new_head)

        if snake[0] == food:
            food = None
            while food is None:
                new_food = [
                    random.randint(1, screen_height - 1),
                    random.randint(1, screen_width - 1),
                ]
                food = new_food if new_food not in snake else None
            window.addch(food[0], food[1], curses.ACS_PI)
        else:
            tail = snake.pop()
            window.addch(tail[0], tail[1], ' ')

        window.addch(snake[0][0], snake[0][1],
                     curses.ACS_CKBOARD)


def main(*args, **kwargs):
    score = wrapper(start_game)
    print("You scored: {:d}!".format(score)
          if score > 0 else "Did you even try?")
    return


if __name__ == '__main__':
    main()
